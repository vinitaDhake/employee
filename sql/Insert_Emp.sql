Insert into emp(emp_id,emp_name,designation,business_unit) values(1,'Veena','Jr. Developer','Insurance');
Insert into emp(emp_id,emp_name,designation,business_unit) values(2,'Meena','Sr. Developer','Insurance');
Insert into emp(emp_id,emp_name,designation,business_unit) values(3,'Teena','Developer','Banking');
Insert into emp(emp_id,emp_name,designation,business_unit) values(4,'Beena','Manager','Insurance');
Insert into emp(emp_id,emp_name,designation,business_unit) values(5,'Reena','Jr. Developer','Banking');
Insert into emp(emp_id,emp_name,designation,business_unit) values(6,'Seema','Developer','Banking');
COMMIT;